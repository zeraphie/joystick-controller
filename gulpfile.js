var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var del = require('del');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var runSequence = require('run-sequence');
var plumber = require('gulp-plumber');
var util = require('gulp-util');

var browserify = require('browserify');
var babelify = require('babelify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');

var src = 'assets';
var dest = 'www/build';

var myPlumber = function() {
    return plumber({
        errorHandler: function(error) {
            util.log(util.colors.red('Unhandled error:\n'), error.toString());
            return this.emit('end');
        }
    });
};

gulp.task('clean:css', function() {
    return del(dest + "/styles");
});

gulp.task('css', ['clean:css'], function() {
    return gulp.src(src + "/styles/*.scss")
        .pipe(sourcemaps.init())
        .pipe(sass({
            includePaths: [src + "/styles"],
            outputStyle: 'compressed',
            sourceMap: true
        }).on('error', sass.logError))
        .pipe(myPlumber())
        .pipe(autoprefixer())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(dest + "/styles"));
});

gulp.task('clean:js', function() {
    return del(dest + "/js");
});

gulp.task('js', ['clean:js'], function() {
    return browserify({
        entries: [
            src + '/js/joystick-components/JoystickElement.js',
            src + '/js/joystick-components/JoystickShaft.js',
            src + '/js/joystick-components/Joystick.js',
            src + '/js/master.js',
        ],
        debug: true
    })
        .transform(babelify)
        .bundle()
        .pipe(source('master.js'))
        .pipe(buffer())
        .pipe(sourcemaps.init({loadMaps: true}))
            .pipe(uglify())
            .pipe(myPlumber())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(dest + '/js'));
});

gulp.task('build', ['css', 'js']);

gulp.task('watch', function() {
    gulp.watch(src + '/styles/**', ['css']);
    return gulp.watch(src + '/js/**', ['js']);
});

gulp.task('default', function (cb) {
    return runSequence('build', 'watch', cb);
});
