// Polyfill for Pointer Events
import "pepjs";

// Get the Joystick class
import Joystick from "./joystick-components/Joystick";

// Setup the Joystick
const joystick = new Joystick('.joystick-base', '.joystick-shaft');

// Attach the events for the joystick
// Can also detach events with the detachEvents function
joystick.attachEvents();

// Lets animate the background colour around using hsl to show the degree of control this has!
// Puns are funny.
joystick.ondeactivate = function(){
    document.body.removeAttribute('style');
};

// Utilize the angle of the joystick in comparison to the center
// as well as how far away it is from the center
joystick.ondrag = function(){
    document.body.style.background = `hsl(${this.shaft.current.angle}, ${this.shaft.current.percentage}%, 50%)`;
};
