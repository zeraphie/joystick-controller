import "velocity-animate";
import JoystickElement from "./JoystickElement";

export default class JoystickShaft extends JoystickElement {
    /**
     * Restrict the joystick shaft's movement to a given boundary
     *
     * @param x
     * @param y
     * @param boundary
     * @returns {{x: number, y: number}}
     */
    clamp(x, y, boundary){
        // Trigonometry time!
        // - Who says what you learn in school won't become useful :D
        let diff = {
            x: x - this.rect.center.x,
            y: y - this.rect.center.y,
        };

        // Get the distance between the cursor and the center
        let distance = Math.sqrt(
            Math.pow(diff.x, 2) + Math.pow(diff.y, 2)
        );

        // Get the angle of the line
        let angle = Math.atan2(diff.x, diff.y);
        // Convert into degrees!
        this.current.angle = 180 - (angle * 180 / Math.PI);

        // If the cursor is distance from the center is
        // less than the boundary, then return the diff
        //
        // Note: Boundary = radius
        if(distance < boundary){
            this.current.percentage = (distance / boundary) * 100;
            return this.current.vector = diff;
        }

        // If it's a longer distance, clamp it!
        this.current.percentage = 100;

        return this.current.vector = {
            x: Math.sin(angle) * boundary,
            y: Math.cos(angle) * boundary
        };
    }

    /**
     * Utilize velocity to animate the position of the joystick shaft to a point
     * in a smooth manner
     *
     * Help: http://velocityjs.org/
     *
     * @param from
     * @param to
     * @param duration
     * @param callback
     * @returns {*}
     */
    move(from, to, duration, callback){
        return Velocity(
            // Select the element to transform
            this.element,

            // Utilize forcefeeding to transform to a value, from a value
            {
                translateX: [to.x, from.x],
                translateY: [to.y, from.y],
                translateZ: 0,
            },

            // Additional Velocity config including the on complete callback
            {
                duration: duration,
                queue: false,
                complete(){
                    if(typeof callback === 'function'){
                        callback();
                    }
                }
            }
        );
    }
}
