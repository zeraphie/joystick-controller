import JoystickElement from "./JoystickElement";
import JoystickShaft from "./JoystickShaft";

export default class Joystick {
    constructor(base, shaft){
        this.state = 'inactive';
        this.base = new JoystickElement(base);
        this.shaft = new JoystickShaft(shaft);

        // Add the hooks
        this.onactivate = function(){};
        this.ondeactivate = function(){};
        this.ondrag = function(){};

        // Make sure the events are addable and removable!
        this.activate = this.activate.bind(this);
        this.deactivate = this.deactivate.bind(this);
        this.drag = this.drag.bind(this);
    }

    /**
     * The animation duration for animating the joystick to the center
     *
     * @returns {number}
     */
    get duration(){
        return 100;
    }

    /**
     * The boundary for the joystick shaft to be limited by
     *
     * @returns {number}
     */
    get boundary(){
        return this.base.rect.radius * 0.75;
    }

    /**
     * Attach all the necessary events to the elements for the joystick to work
     *
     * @returns {Joystick}
     */
    attachEvents(){
        this.base.element.addEventListener('pointerdown', this.activate, false);
        document.addEventListener('pointerup', this.deactivate, false);
        document.addEventListener('pointermove', this.drag, false);

        return this;
    }

    /**
     * Remove all the events that were bound with attachEvents
     *
     * @returns {Joystick}
     */
    detachEvents(){
        this.base.element.removeEventListener('pointerdown', this.activate, false);
        document.removeEventListener('pointerup', this.deactivate, false);
        document.removeEventListener('pointermove', this.drag, false);

        // Make sure to reset everything!
        this.deactivate();

        return this;
    }

    /**
     * Activate the joystick (on clicking the base element)
     *
     * @returns {Joystick}
     */
    activate(){
        this.state = 'active';
        this.base.element.classList.add('active');

        if(typeof this.onactivate === 'function'){
            this.onactivate();
        }

        return this;
    }

    /**
     * Deactivate the joystick on mouseup/touchup as well as if the events are
     * unbound
     *
     * @returns {Joystick}
     */
    deactivate(){
        // Only fire this if the state was active in the first place!
        if(this.state !== 'active'){
            return this;
        }

        this.state = 'inactive';
        this.base.element.classList.remove('active');

        // Make sure to move it back to the center!
        this.shaft.move(
            this.shaft.current.vector,
            this.shaft.original.vector,
            this.duration,
            () => {
                // Reset all the relevant attributes
                this.shaft.element.removeAttribute('style');
                this.shaft.current = this.shaft.original;

                if(typeof this.ondeactivate === 'function'){
                    this.ondeactivate();
                }
            }
        );

        return this;
    }

    /**
     * Move the joystick shaft
     *
     * @param e
     * @returns {Joystick}
     */
    drag(e){
        // If it isn't active, don't do anything
        if(this.state !== 'active'){
            return this;
        }

        // Move the joystick but keep it within the boundaries of the base
        this.shaft.move(
            this.shaft.original.vector,
            this.shaft.clamp(e.clientX, e.clientY, this.boundary),
            0,
            () => {
                if(typeof this.ondrag === 'function'){
                    this.ondrag();
                }
            }
        );

        return this;
    }
}
