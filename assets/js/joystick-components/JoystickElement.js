export default class JoystickElement {
    constructor(selector){
        this.element = document.querySelector(selector);
        this.rect = this.calculateRect();
        this.current = this.original;

        // Recalculate the rect on resizing
        window.onresize = () => {
            this.rect = this.calculateRect();
        }
    }

    /**
     * Make an original array of the values that will be changed on moving the
     * joystick to compare against and to reset
     *
     * @returns {{vector: {x: number, y: number}, angle: number, percentage: number}}
     */
    get original(){
        return {
            vector: {
                x: 0,
                y: 0
            },
            angle: 0,
            percentage: 0
        };
    }

    /**
     * Extend the bounding client rect with a center point and radius
     *
     * @returns {*}
     */
    calculateRect(){
        let rect = this.element.getBoundingClientRect();

        return Object.assign(
            rect,
            {
                center: {
                    x: rect.left + rect.width / 2,
                    y: rect.top + rect.height / 2
                },
                radius: rect.width / 2 // Improve this
            }
        );
    }
}
