# Joystick Controller

This project is a bunch of classes that are utilized to make a "Joystick"-like controller.

The Joystick can be used to manipulate elements by using angles, weighting and direction values, as is shown with the current basic example of animating the background via the usage of hsl

*Note: Adapted from this codepen to be able to be used in general circumstances [https://codepen.io/zephyr/pen/aLXXWX](https://codepen.io/zephyr/pen/aLXXWX)*

## Installation
Everything should work right out of the box if you open up the [index](www/index.html) file, but to use the joystick, take a look at the [master](assets/js/master.js) file

*Note: this requires `pepjs` to be installed*

To mess around with it, simply install the dependencies using npm and run gulp
```bash
npm install
gulp
```

## Notes
1. This uses browserify to make use of the import/export syntax of es6
2. At the moment each module needs to be added to the gulpfile
3. Import files with the `./` prefix in order to make sure it matches from the current directory
